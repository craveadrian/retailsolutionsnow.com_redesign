<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php if ($view == "home") : ?>
		<link rel="canonical" href="<?php echo URL; ?>" />
	<?php endif; ?>
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
	<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="social-media columns-between-center">
					<a href="<?php $this->info("fb_link"); ?>" target="_blank">F</a>
					<a href="<?php $this->info("tt_link"); ?>" target="_blank">L</a>
					<a href="<?php $this->info("yt_link"); ?>" target="_blank">X</a>
					<a href="<?php $this->info("rs_link"); ?>" target="_blank">R</a>
				</div>
				<div class="header-bottom columns-between-center">
					<div class="header-bottom__left col-9">
						<nav>
							<a href="#" id="pull"><img src="public/images/common/mobile-logo.jpg" alt="mobile logo" class="mobile-logo"></a>
							<ul>
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
								<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">GALLERY</a></li>
								<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services">SERVICES</a></li>
								<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">CONTACT</a></li>
							</ul>
						</nav>
					</div>
					<div class="header-bottom__right col-3">
						<?php $this->info(["phone", "tel", "phone"]); ?>
					</div>
				</div>

			</div>
		</div>
	</header>

	<?php //if ($view == "home") : ?>
		<div id="banner">
			<div class="slider">
				<img src="public/images/common/banner-img.jpg" alt="Banner Image 1">
			</div>
			<div class="row">
				<a href="<?php echo URL; ?>"><img src="public/images/common/logo.png" alt="Logo" class="banner-logo"></a>
			</div>
			<div class="banner-bottom columns-between-center">
				<div class="banner-bottom__left col-10">
					<h3>Retail Facilities Maintenance & Construction</h3>
					<p>Fast, full-range facilities maintenance, construction, and handyman services throughout the San Francisco bay area.</p>
				</div>
				<div class="banner-bottom__right col-2">
					<a href="contact#content" class="btn"><span class="text">Estimate</span><span class="background"></span></a>
				</div>
			</div>
		</div>
	<?php //endif; ?>