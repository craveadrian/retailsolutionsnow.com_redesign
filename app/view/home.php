<div id="welcome">
	<div class="row">
		<div class="wlc-box">
			<h1>Retail Solution Now</h1>
			<p>At Retail Solutions Now, we are a licensed general contractor, offering fast and full-range retail construction, facilities maintenance, and handyman services to repair, rebuild and maintain your retail store throughout the entire San Francisco Bay area.</p>

			<p>Our professional staff are with you every step of the way as we provide general repairs, flooring installations, painting, and more.</p>

			<p>General contractor, fully licensed and insured by the California State License Board (CSLB)</p>
			<a href="contact#content" class="btn"><span class="text">CONTACT US</span><span class="background"></span></a>
		</div>
	</div>
</div>
<div id="retail">
	<div class="row columns-between-center">
		<div class="retail__left col-5">
			<div class="retail-header">
				<h2>Quality Retail Solutions</h2>
				<p>MORE THAN 15 YEARS OF EXPERIENCE</p>
			</div>
		</div>
		<div class="retail__right columns-between-stretch col-7">
			<div class="retail__box repair col-4">
				<p>REPAIR</p>
			</div>
			<div class="retail__box rebuild col-4">
				<p>REBUILD</p>
			</div>
			<div class="retail__box maintain col-4">
				<p>MAINTAIN</p>
			</div>
		</div>
	</div>
</div>
<div id="solutions">
	<h2>Our Solutions</h2>
	<div class="row columns-between-top">
		<div class="solutions__left col-8">
			<ul>
				<li>HANDYMAN</li>
				<li>CARPENTRY</li>
				<li>GENERAL REPAIRS</li>
				<li>FLOORING</li>
				<li>DRYWALL</li>
				<li>LIGHTING</li>
				<li>PAINTING</li>
			</ul>
		</div>
		<div class="solutions__right col-4">
			<p>We provide a wide variety of solutions for your retail establishment ranging from project management to handyman services. We offer scheduled maintenance calls as well as emergency services and our professional staff can spruce up your retail spaces.</p>
			<a href="contact#content" class="btn"><span class="text">CONTACT US</span><span class="background"></span></a>
		</div>
	</div>
</div>
<div id="recent">
	<div class="row columns-between-stretch">
		<img src="public/images/common/recent-img1.jpg" alt="Recent Project Image 1" class="recent__img col-2">
		<div class="recent__button col-8">
			<a href="gallery#content"><span>Click To View Our Recent Projects</span></a>
		</div>
		<img src="public/images/common/recent-img2.jpg" alt="Recent Project Image 2" class="recent__img col-2">
	</div>
</div>
<div id="quote1">
	<div class="row columns-between-center">
		<img src="public/images/common/quote1-img1.png" alt="Quote Image 1" class="quote1__image1 col-4">
		<div class="quote1__mid col-6">
			<h2>Save Time and Money</h2>
			<p>Whether large or small, our experience in retail construction projects ensures your job is done right, while saving you time, money, and aggravation. We take pride in offering quality craftsmanship at reasonable rates and are dedicated to your needs each and every day.</p>
		</div>
		<img src="public/images/common/quote1-img2.png" alt="Quote Image 2" class="quote1__image2 col-4">
	</div>
</div>
<div id="contact">
	<div class="row columns-between-top">
		<div class="contact__left col-6">
			<div class="form-box">
			<h2>Estimate</h2>
			<p>Please contact us via email and we will get back with you as soon as we can with a reply. Thank you!</p>
			<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<label><span class="ctc-hide">Name</span>
				<input type="text" name="name" placeholder="Name:">
			</label>
			<label><span class="ctc-hide">Phone</span>
				<input type="text" name="phone" placeholder="Phone:">
			</label>
			<label><span class="ctc-hide">Email</span>
				<input type="text" name="email" placeholder="Email:">
			</label>
			<label><span class="ctc-hide">Message</span>
				<textarea name="message" cols="30" rows="10" placeholder="Message / Questions:"></textarea>
			</label>
			<div class="form-bottom">
				<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
				<div class="g-recaptcha"></div>
				<label>
					<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
				</label><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label>
					<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
				</label>
				<?php endif ?>
			</div>
			
			<button type="submit" class="ctcBtn btn" disabled><span class="text">SUBMIT FORM</span><span class="background"></span></button>
		</form>
			</div>
		</div>
		<div class="contact__right col-6">
			<h2>About Us</h2>
			<p class="first">
			Enjoy quality, affordable Contractor Services, Facilities Maintenance, Construction, and Handyman Services for your retail establishment.
			</p>
			<p class="second">
			Facilities Maintenance & Construction
			</p>
			<p class="third">At Retail Solutions Now, we pride ourselves on quality craftsmanship, and our professional staff makes sure the work is performed correctly the first time.</p>

			<p class="third">Our skilled professionals are punctual and clean, always providing high-quality professional services in fast fashion to keep your business up and running. We offer reasonable rates and a proven track record of quality. No matter what the size and scope of your project, the experienced professionals at Retail Solutions Now will plan, construct, and complete the work to exceed your expectations.</p>

			<div class="contact__links columns-between-top">
			<a href="contact#content" class="btn col-5"><span class="text">CONTACT US</span><span class="background"></span></a>
			<a href="https://www.bbb.org/"><img src="public/images/common/bbb.png" alt="BBB" class="bbb col-5"></a>
			</div>
			
		</div>	
	</div>
</div>
<div id="contact-bottom">
	<div class="row">
		<div class="cb-container">
			<h4>The Retail Repair Specialists</h4>
			<p>Facilities Maintenance & Construction</p>
		</div>
	</div>
</div>